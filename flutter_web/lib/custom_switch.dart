import 'package:flutter/material.dart';

class CustomSwitch extends StatefulWidget {
  const CustomSwitch({super.key});

  @override
  State<CustomSwitch> createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch> {
  bool isMale = true;
  String gender = 'Masculino';
  @override
  Widget build(BuildContext context) {
    return Switch(
      value: isMale,
      onChanged: (bool male) {
        setState(() {
          if (male) {
            isMale = true;
            gender = 'Masculino';
          } else {
            isMale = false;
            gender = 'Feminino';
          }
        });
      },
    );
  }
}
