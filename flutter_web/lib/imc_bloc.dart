import 'dart:async';

import 'package:web/imc_input_model.dart';
import 'package:web/assets_images.dart';

import 'imc_output_model.dart';

enum ImcEvent {
  calculate,
}
// abstract class ImcEvent {}

// class CalculateImcEvent implements ImcEvent {}

class ImcBloc {
  late ImcOutputModel _imcModel;

  final StreamController<ImcOutputModel> _stateController =
      StreamController<ImcOutputModel>();
  final StreamController<ImcEvent> _eventController =
      StreamController<ImcEvent>();

  ImcOutputModel get initialData => _imcModel;
  Stream<ImcOutputModel> get state => _stateController.stream;

  ImcBloc() {
    _eventController.stream.listen((event) {
      _handleEvent(event);
    });
  }

  void calculate(ImcInputModel imcInputModel) {
    double? alturaDouble = imcInputModel.altura;
    double? pesoDouble = imcInputModel.peso;
    double result = pesoDouble / (alturaDouble * alturaDouble);
    if (result > 40) {
      _imcModel = ImcOutputModel(
        description:
            'Aqui o sinal é vermelho, com forte probabilidade de já existirem doenças muito graves associadas. O tratamento deve ser ainda mais urgente.',
        numberResult: result,
        result: 'Obesidade grau III',
        imageUrl: AssetsImages.obesidade3,
      );
    } else if (result > 35 && result < 39.9) {
      _imcModel = ImcOutputModel(
        description:
            'Mesmo que seus exames aparentem estar normais, é hora de se cuidar, iniciando mudanças no estilo de vida com o acompanhamento próximo de profissionais de saúde.',
        numberResult: result,
        result: 'Obesidade grau II',
        imageUrl: AssetsImages.obesidade2,
      );
    } else if (result > 30 && result < 34.9) {
      _imcModel = ImcOutputModel(
        description:
            'Sinal de alerta! Chegou na hora de se cuidar, mesmo que seus exames sejam normais. Vamos dar início a mudanças hoje! Cuide de sua alimentação. Você precisa iniciar um acompanhamento com nutricionista e/ou endocrinologista.',
        numberResult: result,
        result: 'Obesidade grau I',
        imageUrl: AssetsImages.obesidade1,
      );
    } else if (result > 25 && result < 29.9) {
      _imcModel = ImcOutputModel(
        description:
            'Ele é, na verdade, uma pré-obesidade e muitas pessoas nessa faixa já apresentam doenças associadas, como diabetes e hipertensão. Importante rever hábitos e buscar ajuda antes de, por uma série de fatores, entrar na faixa da obesidade pra valer.',
        numberResult: result,
        result: 'Sobrepeso',
        imageUrl: AssetsImages.sobrepeso,
      );
    } else if (result > 18.6 && result < 24.9) {
      _imcModel = ImcOutputModel(
        description:
            'Que bom que você está com o peso normal! E o melhor jeito de continuar assim é mantendo um estilo de vida ativo e uma alimentação equilibrada.',
        numberResult: result,
        result: 'Normal',
        imageUrl: AssetsImages.normal,
      );
    } else {
      _imcModel = ImcOutputModel(
        description:
            'Procure um médico. Algumas pessoas têm um baixo peso por características do seu organismo e tudo bem. Outras podem estar enfrentando problemas, como a desnutrição. É preciso saber qual é o caso.',
        numberResult: result,
        result: 'Abaixo do normal',
        imageUrl: AssetsImages.abaixo,
      );
    }

    _eventController.add(ImcEvent.calculate);
  }

  _handleEvent(ImcEvent event) {
    if (event == ImcEvent.calculate) {
      _imcModel = _imcModel;
      _stateController.add(_imcModel);
    }
  }
}
