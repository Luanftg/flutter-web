class ImcInputModel {
  final bool isFemale;

  final double altura;
  final double peso;

  ImcInputModel({
    required this.altura,
    required this.peso,
    required this.isFemale,
  });

  copyWith({
    double? altura,
    double? peso,
    bool? isMale,
    bool? isFemale,
    String? description,
  }) {
    return ImcInputModel(
      altura: altura ?? this.altura,
      peso: peso ?? this.peso,
      isFemale: isFemale ?? this.isFemale,
    );
  }
}
