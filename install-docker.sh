 #!/bin/bash
 
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

cd ../../vagrant_data
docker build -t flutter-web-container .
docker run --name flutter-web-container -v $(pwd):/web flutter-web-container