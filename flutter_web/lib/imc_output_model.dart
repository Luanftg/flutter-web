class ImcOutputModel {
  final String description;
  final double numberResult;
  final String result;
  final String imageUrl;

  ImcOutputModel({
    required this.description,
    required this.numberResult,
    required this.result,
    required this.imageUrl,
  });
}
