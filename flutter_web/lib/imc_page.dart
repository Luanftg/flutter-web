import 'package:flutter/material.dart';
import 'package:web/custom_switch.dart';
import 'package:web/imc_bloc.dart';
import 'package:web/imc_input_model.dart';
import 'package:web/imc_output_model.dart';
import 'package:web/responsive_layout.dart';

class ImcPage extends StatefulWidget {
  const ImcPage({super.key, required this.title});

  final String title;

  @override
  State<ImcPage> createState() => _ImcPageState();
}

class _ImcPageState extends State<ImcPage> {
  bool isMale = true;
  String gender = 'Masculino';
  String genderDescription =
      'A medida da circunferência abdominal reﬂete de forma indireta o conteúdo de gordura entre os órgãos da região. A Organização Mundial da Saúde estabelece que a medida igual ou superior a 94 cm em homens e 80 cm em mulheres.';
  TextEditingController altura = TextEditingController();
  TextEditingController peso = TextEditingController();
  GlobalKey key = GlobalKey();

  late ImcBloc imcBloc;

  @override
  void initState() {
    super.initState();
    imcBloc = ImcBloc();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveLayout(
        desktop: Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
            centerTitle: true,
          ),
          body: Center(
            child: SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: Column(
                children: <Widget>[
                  const SizedBox(height: 16),
                  const Text(
                      'O índice de massa corporal de um adulto é o seu peso em quilos (por exemplo, 80 kg), dividido por sua altura ao quadrado (vamos imaginar, 1,80 m x 1,80 m).',
                      textAlign: TextAlign.justify),
                  const SizedBox(height: 16),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text('Masculino'),
                      CustomSwitch(),
                      const Text('Feminino'),
                    ],
                  ),
                  const SizedBox(height: 16),
                  TextFormField(
                    decoration:
                        const InputDecoration(labelText: 'Insira a sua altura'),
                    controller: altura,
                  ),
                  const SizedBox(height: 16),
                  TextFormField(
                    decoration:
                        const InputDecoration(labelText: 'Insira o seu peso'),
                    controller: peso,
                  ),
                  const SizedBox(height: 16),
                  ElevatedButton(
                    onPressed: () {
                      final ImcInputModel imcInputModel = ImcInputModel(
                          altura: double.tryParse(altura.text) ?? 0,
                          peso: double.tryParse(peso.text) ?? 0,
                          isFemale: !isMale);
                      imcBloc.calculate(imcInputModel);
                    },
                    child: const Text('Calcular'),
                  ),
                  const SizedBox(height: 16),
                  StreamBuilder<ImcOutputModel>(
                      stream: imcBloc.state,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          final result = snapshot.data!;
                          return Column(
                            children: [
                              Text(result.numberResult.toStringAsFixed(2)),
                              const SizedBox(height: 8),
                              Text(
                                snapshot.data!.result,
                                style:
                                    Theme.of(context).textTheme.headlineMedium,
                              ),
                              const SizedBox(height: 8),
                              Image.asset(
                                'assets/${snapshot.data!.imageUrl}',
                              ),
                              const SizedBox(height: 8),
                              Text(
                                snapshot.data!.description,
                                textAlign: TextAlign.justify,
                              ),
                            ],
                          );
                        }
                        return const SizedBox.shrink();
                      }),
                ],
              ),
            ),
          ),
        ),
        mobile: Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
            centerTitle: true,
          ),
          body: Center(
            child: SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: Column(
                children: <Widget>[
                  const SizedBox(height: 16),
                  const Text(
                      'O índice de massa corporal de um adulto é o seu peso em quilos (por exemplo, 80 kg), dividido por sua altura ao quadrado (vamos imaginar, 1,80 m x 1,80 m).',
                      textAlign: TextAlign.justify),
                  const SizedBox(height: 16),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Masculino'),
                      CustomSwitch(),
                      Text('Feminino'),
                    ],
                  ),
                  const SizedBox(height: 16),
                  TextFormField(
                    decoration:
                        const InputDecoration(labelText: 'Insira a sua altura'),
                    controller: altura,
                  ),
                  const SizedBox(height: 16),
                  TextFormField(
                    decoration:
                        const InputDecoration(labelText: 'Insira o seu peso'),
                    controller: peso,
                  ),
                  const SizedBox(height: 16),
                  ElevatedButton(
                    onPressed: () {
                      final ImcInputModel imcInputModel = ImcInputModel(
                          altura: double.tryParse(altura.text) ?? 0,
                          peso: double.tryParse(peso.text) ?? 0,
                          isFemale: !isMale);
                      imcBloc.calculate(imcInputModel);
                    },
                    child: const Text('Calcular'),
                  ),
                  const SizedBox(height: 16),
                  StreamBuilder<ImcOutputModel>(
                      stream: imcBloc.state,
                      builder: (context, snapshot) {
                        return Column(
                          children: [
                            Text(
                              // imcController.result,
                              snapshot.data?.result ?? '',
                              style: Theme.of(context).textTheme.headlineMedium,
                            ),
                            const SizedBox(height: 8),
                            Text(
                              snapshot.data?.description ?? '',
                              textAlign: TextAlign.justify,
                            ),
                          ],
                        );
                      }),
                ],
              ),
            ),
          ),
        ));
  }
}
